<div align="center">
  <img src="/images/logo.png" alt="logo" width="200">
</div>

<div align="center">
  <h1>Sinapsis Coding Challenge</h1>
  <strong>A simple API that generates thumbnails from a source image</strong>
  <br>
  <sub>
    Built by <a href="https://gitlab.com/cdipietro11">Carlos A. Di Pietro</a>
  </sub>
  <br>
</div>

<div align=center>
    <img src="/images/screenshot.gif" alt="screenshot" width=80% height=80%/>
</div>


## Table of Contents
- [Overview](#overview)
  - [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Versioning](#versioning)
- [Release History](#release_history)
- [Links](#links)
- [License](#license)


## Overview
A brief description of your project, what it is used for and how does life get
awesome when someone starts to use it.

### Features
A few of the things you can do with your project:
- Feature 1
- Feature 2
- ...
- Feature n


## Installation
Follow the [installation instructions](INSTALL.md) for a step by step
installation guide.


## Usage
Explain how to use your project.


## Versioning
We use [SemVer](http://semver.org/) versioning scheme. For available versions,
check the [tags](https://gitlab.com/cdipietro11/repo_template/tags) on
this repository.


## Release History
To see changes across released versions, please read the [changelog](CHANGELOG.md) file.


## Links
- [Project homepage](README.md)
- [Repository](https://gitlab.com/cdipietro11/repo_template/tree/master)
- [Documentation]()
- [Issue tracker](https://gitlab.com/cdipietro11/repo_template/issues)


## License
This project is licensed under the [MIT License](https://opensource.org/licenses/MIT). Please see the [LICENSE.md](LICENSE.md) file for more details.
