import os
from yaml import load


class Config(dict):
    """Dictionary-like class for loading and storing application configuration values from configuration files and
    environment variables.
    """

    def load(self):
        """Loads configuration values from configuration files. It chooses the configuration file to be used
        based on the value of the `RUN_MODE` environment variable, which can be one of the following:
            - PROD
            - DEV
            - TEST

        So, for example, if `RUN_MODE=DEV`, configurations will be loaded from a file named as `config-dev.yml`.
        """
        run_mode = os.environ.get('RUN_MODE', 'dev').lower()
        config_path = os.path.dirname(os.path.realpath(__file__))
        config_filename = 'config.yml' if run_mode == 'prod' else 'config-{}.yml'.format(run_mode)
        config_file = os.path.join(config_path, config_filename)

        if os.path.isfile(config_file):
            with open(config_file, encoding='utf8') as cfg:
                config_data = load(cfg)
                for key, value in config_data.items():
                    self[key] = value

        self._load_envvars()

    def _load_envvars(self):
        """Loads configuration values from environment variables
        """
        self['images_bucket'] = os.getenv('IMAGES_BUCKET_NAME')
        self['resize_orders_queue'] = os.getenv('RESIZE_ORDERS_QUEUE')


config = Config()
config.load()
