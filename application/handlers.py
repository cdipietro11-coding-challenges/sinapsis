import base64
import json
import uuid

import application.common.exceptions as exc
import application.common.responses as rsp
import application.utils.image as img
import application.utils.sqs as sqs
import application.utils.s3 as s3

from application.config import config


@rsp.format_errors
def upload_image(event, context):
    """Uploads the image received in the request into an S3 bucket and publishes resizing orders into an SQS Queue.

    :param event: Event that triggered the lambda handler
    :type event: dict
    :param context: Runtime information of the lambda handler
    :type context: LambdaContext
    :return: An HTTP response with status code 200
    :rtype: responses.HTTPResponse

    :raises BadRequestError: If request has 'Content-Type': 'application/json' but payload does not include
                             an 'image_url' key
    :raises BadRequestError: If provided image has a not accepted format
    :raises BadRequestError: If provided image exceeds accepted size limit
    """

    # Get image source from request body or URL
    if bool(event["isBase64Encoded"]):
        image_src = base64.b64decode(event['body'])
    else:
        payload = json.loads(event['body'])
        image_src = payload.get('image_url')
        if not image_src:
            raise exc.BadRequestError('Key "image_url" missing.')

    # Load image in memory
    image = img.load_image(image_src)

    # Check image has an accepted format
    if image.format not in config['input_images']['accepted_formats']:
        raise exc.BadRequestError('Unsupported image format.')

    # Check image does not exceeds the accepted size
    image_size = img.get_size(image)
    if image_size > config['input_images']['size_limit']:
        raise exc.BadRequestError('Image exceeds the 5 MB limit.')

    # Upload image to S3
    image_name = uuid.uuid1()
    image_ext = img.get_extension(image)
    image_key = 'intake/{name}.{ext}'.format(name=image_name, ext=image_ext)
    s3.upload_image(config['images_bucket'], image_key, image)

    # Build resizing orders and pre-signed URLs for each expected thumbnail
    orders = []
    thumbnails = []
    for (width, height) in config['output_images']['dimensions']:
        dimensions = '{}x{}'.format(width, height)
        thumbnail_key = '{dim}/{name}_{dim}.{ext}'.format(dim=dimensions, name=image_name, ext=image_ext)
        thumbnails.append(
            {
                'key': thumbnail_key,
                'dimensions': dimensions,
                'url': s3.get_object_presigned_url(config['images_bucket'], thumbnail_key)
            }
        )
        orders.append(
            json.dumps({
                'image_key': image_key,
                'new_dimensions': (width, height),
                'thumbnail_key': thumbnail_key
            })
        )

    # Publish resizing orders
    sqs.publish_resize_orders(config['resize_orders_queue'], orders)

    return rsp.HTTPResponse(status_code=200, body={'thumbnails': thumbnails})


@rsp.format_errors
def supported_info(event, context):
    """Returns information about supported input image formats and output image dimensions

    :param event: Event that triggered the lambda handler
    :type event: dict
    :param context: Runtime information of the lambda handler
    :type context: LambdaContext
    :return: An HTTP response with status code 200
    :rtype: responses.HTTPResponse
    """
    rsp_body = {
        'input': {
            'formats': config['input_images']['accepted_formats']
        },
        'output': {
            'dimensions': [{'width': w, 'height': h} for w, h in config['output_images']['dimensions']]
        }
    }
    return rsp.HTTPResponse(status_code=200, body=rsp_body)


def resize_image(event, context):
    """Resizes an image after being triggered by a new message queued in the Resize Order SQS Queue.

    :param event: Event that triggered the lambda handler
    :type event: dict
    :param context: Runtime information of the lambda handler
    :type context: LambdaContext
    :return: An event response
    :rtype: dict
    """
    message = event['Records'][0]
    resize_order = json.loads(message['body'])

    image_key = resize_order['image_key']
    thumbnail_key = resize_order['thumbnail_key']
    width, height = resize_order['new_dimensions']
    bucket_name = config['images_bucket']

    image = s3.get_image(bucket_name, image_key)
    thumbnail = img.resize(image, width=width, height=height, clone=False)
    s3.upload_image(bucket_name, thumbnail_key, thumbnail)

    return {
        "message": "Image {} resized to {}x{} and saved as {}".format(image_key, width, height, thumbnail_key),
        "event": event
    }
