import application.utils.image as img

import boto3


def upload_image(bucket_name, key, image):
    """Uploads an image to an S3 bucket using the provided key.

    :param bucket_name: Name of the bucket were image has to be stored
    :type bucket_name: str
    :param key: Identifier to be assigned to uploaded the image
    :str key: str
    :param image: An image object
    :type image: PIL.Image.Image
    :return: Data about S3 created object
    :rtype: dict
    """
    s3_object = {
        'Key': key,
        'ContentType': img.get_MIME_type(image),
        'Body': img.to_bytestream(image).getvalue()
    }

    s3_client = boto3.client('s3')
    s3_client.put_object(Bucket=bucket_name, **s3_object)
    return s3_object


def get_image(bucket_name, key):
    """Gets an image from an S3 bucket.

    :param bucket_name: Name of the bucket were the image is stored
    :type bucket_name: str
    :param key: Identifier of the object holding the image
    :type key: str
    :return: An image object
    :rtype: PIL.Image.Image
    """
    s3_client = boto3.client('s3')
    object = s3_client.get_object(Bucket=bucket_name, Key=key)
    stream = object['Body'].read()
    image = img.load_image(stream)
    return image


def get_object_presigned_url(bucket_name, key, expiration=604800):
    """Creates a pre-signed URL for the S3 object stored in the `bucket_name` bucket under the `key` identifier.

    :param bucket_name: Name of the bucket were the object is stored
    :type bucket_name: str
    :param key: Identifier of the object
    :type key: str
    :param expiration: Number of seconds util URL expires (min: 1, max: 7 days)
    :type expiration: int
    :return: A pre-signed URL
    :rtype: str
    """
    s3_client = boto3.client('s3')
    presigned_url = s3_client.generate_presigned_url(
        'get_object',
        Params={'Bucket': bucket_name, 'Key': key},
        ExpiresIn=expiration
    )
    return presigned_url
