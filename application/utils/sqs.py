import boto3


def publish_resize_orders(queue_url, messages, retry=True):
    """Publishes a set of messages into an SQS queue.

    :param queue_url: Name of the queue were messages are going to be published
    :type queue_url: str
    :param messages: List of messages to be published
    :type messages: list(str)
    :param retry: If True, retries once publishing those messages that failed at first attempt
    :type retry: bool
    """
    sqs_client = boto3.client('sqs')

    entries = [{'Id': str(i), 'MessageBody': msg} for i, msg in enumerate(messages)]

    response = sqs_client.send_message_batch(QueueUrl=queue_url, Entries=entries)

    if response.get('Failed') and retry:
        for failed in response['Failed']:
            message = entries[failed[int('Id')]]
            sqs_client.send_message(QueueUrl=queue_url, MessageBody=message['MessageBody'])
