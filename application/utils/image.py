import io
import os

import requests
from PIL import Image


def load_image(image_src):
    """Loads an image from a source into an Pillow Image object.
    Sources can be one of the following:
        - A path to a local image file
        - A URL to an image on the Internet
        - A byte array containing the image binary data
    If `image_src` is not one of the above, then `None` is returned.

    :param image_src: Source to load the image from
    :type image_src: str | bytes
    :return: An image object
    :rtype: PIL.Image.Image | NoneType
    """
    if isinstance(image_src, str):
        if os.path.isfile(image_src):
            image = Image.open(image_src)
        else:
            response = requests.get(image_src)
            image = Image.open(io.BytesIO(response.content))

    elif isinstance(image_src, bytes):
        image = Image.open(io.BytesIO(image_src))

    else:
        image = None

    return image


def to_bytestream(image, format=None):
    """Converts a Pillow Image object into a byte stream using the specified format.
    If no format is given, then the format of the image object is used instead.

    :param image: An image object
    :type image: PIL.Image.Image
    :param format: The format to be used while converting it to bytestream
    :format format: str
    :return: A byte stream
    :rtype: io.BytesIO
    """
    stream = io.BytesIO()
    image.save(stream, format=format if format else image.format)
    return stream


def get_size(image):
    """Gets the size in bytes of the given image.

    :param image: An image object
    :type image: PIL.Image.Image
    :return: The size in bytes of the image
    :rtype: int
    """
    stream = to_bytestream(image, format=image.format)
    return stream.tell()


def get_extension(image):
    """Gets the filename extension of the given image based on its format.

    :param image: An image object
    :type image: PIL.Image.Image
    :return: The image filename extension if exists. Otherwise, returns None
    :rtype: str | NoneType
    """

    if hasattr(image, 'format'):
        return 'jpg' if image.format is 'JPEG' else image.format.lower()

    return None


def get_MIME_type(image):
    """Gets the MIME type of the given image based on its format.

    :param image: An image object
    :type image: PIL.Image.Image
    :return: The MIME type of the image if exists. Otherwise, returns None
    :rtype: str | NoneType
    """

    if hasattr(image, 'get_format_mimetype'):
        return image.get_format_mimetype()

    if hasattr(image, 'format'):
        return 'image/' + image.format.lower()

    return None


def resize(image, scale=None, width=None, height=None, clone=True):
    """Resize the given image based on the parameters provided as input.
    Image can be resized either by `scale` or either by providing a desired `width` or `height` or both of them.

    :param image: Image to be resized
    :type image: PIL.Image.Image
    :param scale: The desired scale to be applied to the image for resizing
    :type scale: int
    :param width:  The desired width for the resized image
    :type width: int
    :param height: The desired height for the resized image
    :type height: int
    :param clone: Boolean flag that indicates whether the input image has to be cloned or resized in place.
    :type clone: bool
    :return: The resized image
    :rtype: PIL.Image.Image
    """
    img_width, img_height = image.size

    # Figure out new dimensions of the image
    if scale:
        new_size = (img_width * scale, img_height * scale)
    elif width and height:
        new_size = (width, height)
    elif width:
        new_size = (width, img_height)
    elif height:
        new_size = (img_width, height)
    else:
        return image

    if clone:
        # Clone image and copy format metadata
        resized = image.copy()
        resized.format = image.format
        resized.format_description = image.format_description
        resized.get_format_mimetype = image.get_format_mimetype
    else:
        resized = image

    # Resize image
    resized.thumbnail(new_size, Image.ANTIALIAS)

    return resized
