class SinapsisError(Exception):
    """Base exception class for all errors raised within the Sinapsis Microservice."""
    pass


class HTTPError(SinapsisError):
    """Base exception class for all HTTP errors raised within the Sinapsis Microservice."""
    pass


class BadRequestError(HTTPError):
    """Exception class for errors produced due to bad HTTP requests."""
    pass


class UnauthorizedError(HTTPError):
    """Exception class for errors produced when Authentication has failed or has not yet been provided."""
    pass


class NotFoundError(HTTPError):
    """Exception class for errors produced when requested resource is not found."""
    pass


class BadGatewayError(HTTPError):
    """Exception class for errors produced when received an invalid response from another server."""
    pass
