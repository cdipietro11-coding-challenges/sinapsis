import json
from functools import wraps

import application.common.exceptions as exc


class HTTPResponse(dict):
    """Base class for HTTP response."""

    def __init__(self, status_code=200, body={}, extra_headers={}):
        """HTTPResponse initialization method

        :param status_code: HTTP response status code
        :type status_code: int
        :param body: Dictionary to be converted into JSON-string and placed as body of the response
        :type body: object
        :param extra_headers: Dictionary of extra HTTP headers to be included in the response
        :type extra_headers: dict
        """
        super().__init__()
        headers = {'Content-Type': 'application/json'}
        headers.update(extra_headers)
        body_str = json.dumps(body)
        self.update(
            {
                'statusCode': status_code,
                'headers': headers,
                'body': body_str
            }
        )


def format_errors(fn):
    """Wrapper for HTTP API handlers that catches meaningful exceptions and formats HTTP errors accordingly.

    :param fn: An HTTP handler function
    :type fn: types.FunctionType
    :return: A wrapped function that formats HTTP errors bases on exceptions caught
    :rtype: types.FunctionType
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            rv = fn(*args, **kwargs)
        except exc.BadRequestError as e:
            response = HTTPResponse(
                status_code=400,
                body={
                    "description": "Bad Request: Cannot process request due to missing or malformed parameters",
                    "error": str(e)
                }
            )
            return response
        except exc.UnauthorizedError as e:
            response = HTTPResponse(
                status_code=401,
                body={
                    "description": "Unauthorized: Authentication has failed or has not yet been provided",
                    "error": str(e)
                }
            )
        except exc.NotFoundError as e:
            response = HTTPResponse(
                status_code=404,
                body={
                    "description": "Resource not found",
                    "error": str(e)
                }
            )
            return response
        except exc.BadGatewayError as e:
            response = HTTPResponse(
                status_code=502,
                body={
                    "description": "Bad Gateway: Received an invalid response from another server",
                    "error": str(e)
                }
            )
            return response
        return rv
    return wrapper
